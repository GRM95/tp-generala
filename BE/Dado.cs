﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Dado
    {
        private bool flag;

        public bool Flag
        {
            get { return flag; }
            set { flag = value; }
        }

        private int valorNumero;

        public int ValorNumero
        {
            get { return valorNumero; }
            set { valorNumero = value; }
        }


        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

    }
}
