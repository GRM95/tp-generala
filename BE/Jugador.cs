﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Jugador
    {

        public Jugador(int idj, string usr, string nom, string apell, string clav, string fc, bool habi, string tj, int pg, int pe, int pp, bool log)
        {
            IdJugador = idj;
            usuario = usr;
            nombre = nom;
            apellido = apell;
            clave = clav;
            fechaCreacion = fc;
            habilitado = habi;
            tiempoJugado = tj;
            partidasGanadas = pg;
            partidasEmpatadas = pe;
            partidasPerdidas = pp;
            logeado = log;
        }

        public Jugador(string usr, string clav)
        {
            usuario = usr;
            clave = clav;
        }

        private int idJugador;

        public int IdJugador
        {
            get { return idJugador; }
            set { idJugador = value; }
        }


        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string clave;

        public string Clave
        {
            get { return clave; }
            set { clave = value; }
        }


        private string fechaCreacion;

        public string FechaCreacion
        {
            get { return fechaCreacion; }
            set { fechaCreacion = value; }
        }

        private bool habilitado;

        public bool Habilitado
        {
            get { return habilitado; }
            set { habilitado = value; }
        }

        private string tiempoJugado;

        public string Tiempojugado
        {
            get { return tiempoJugado; }
            set { tiempoJugado = value; }
        }

        private int partidasGanadas;

        public int PartidasGanadas
        {
            get { return partidasGanadas; }
            set { partidasGanadas = value; }
        }

        private int partidasEmpatadas;

        public int PartidasEmpatadas
        {
            get { return partidasEmpatadas; }
            set { partidasEmpatadas = value; }
        }

        private int partidasPerdidas;

        public int PartidasPerdidas
        {
            get { return partidasPerdidas; }
            set { partidasPerdidas = value; }
        }

        private bool logeado;

        public bool Logeado
        {
            get { return logeado; }
            set { logeado = value; }
        }
    }
}
