﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;

namespace DAL
{
   public  class MP_JUGADOR
    {
        public bool flag;

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Jugador jugador )
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", jugador.Nombre));
            acceso.Escribir("JUGADOR_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Jugador jugador )
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", jugador.Nombre));
            parameters.Add(acceso.CrearParametro("@id", jugador.IdJugador));
            acceso.Escribir("JUGADOR_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Jugador jugador)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id", jugador.IdJugador));
            acceso.Escribir("JUGADOR_BORRAR", parameters);

            acceso.Cerrar();
        }


        public bool Logear(BE.Jugador jugador)
        {
            flag = false;
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@user", jugador.Usuario));
            parameters.Add(acceso.CrearParametro("@pass", jugador.Clave));
            DataTable tabla =  acceso.Leer("LOGEAR_USUARIO", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                if (registro[1].ToString() == null)
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }
            }
            return flag;
        }


        public  List<BE.Jugador> Listar()
        {
            List<BE.Jugador> lista = new List<BE.Jugador>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("JUGADOR_LISTAR");
            acceso.Cerrar();


            foreach (DataRow registro in tabla.Rows)
            {
                BE.Jugador p = new BE.Jugador(int.Parse(registro[0].ToString()), registro[1].ToString(), registro[2].ToString(),
                                                registro[3].ToString(), registro[4].ToString(), registro[5].ToString(),bool.Parse(registro[6].ToString()),
                                                registro[7].ToString(),int.Parse(registro[8].ToString()),int.Parse(registro[9].ToString()),
                                                int.Parse(registro[10].ToString()),bool.Parse(registro[10].ToString()));

                p.Nombre = registro[1].ToString();
                p.IdJugador = int.Parse(registro[0].ToString());
                lista.Add(p);
            }
            return lista;
        }




    }
}
