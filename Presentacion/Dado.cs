﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;

namespace Presentacion
{
    public partial class Dado : UserControl
    {
            
        public Dado()
        {
            InitializeComponent();
        }

        public void EstablecerImagen(int valor)
        {

            switch (valor){
                case 1:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\Dice1.png");
                        break;
                    }
                case 2:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\Dice2.png");
                        break;
                    }
                case 3:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\Dice3.png");
                        break;
                    }
                case 4:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\Dice4.png");
                        break;
                    }
                case 5:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\Dice5.png");
                        break;
                    }
                case 6:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\Dice6.png");
                        break;
                    }
                case 7:
                    {
                        pictureBox1.Image = Image.FromFile(".\\imagenes\\dice-cancel.png");
                        
                        break;
                    }

            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            EstablecerImagen(7);
            
        }
    }
}
