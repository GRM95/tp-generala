﻿using BE;
using BLL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class MesaJuego : Form
    {

        BE.Puntaje puntaje = new BE.Puntaje();
        

        List<BE.Dado> Dados1 = new List<BE.Dado>();



        BLL.Puntaje gestorPuntaje = new BLL.Puntaje();

        BLL.Turno gestor = new BLL.Turno();
        BLL.Partida gestorPartida = new BLL.Partida();
        BLL.Dado gestorDado = new BLL.Dado();
        BLL.Jugador gestorJugador = new BLL.Jugador();


        private int conteo;
        public int lanzamientos = 0;

        BE.Jugador jugador1;
        BE.Jugador jugador2;
        

        public void cargarJugadores(BE.Jugador usuario1, BE.Jugador usuario2)
        {
            jugador1 = usuario1;
            jugador2 = usuario2;

        }

        public MesaJuego()
        {
            InitializeComponent();
            conteo = 20;
        }

        private void MesaJuego_Load(object sender, EventArgs e)
        {

            dado1.Visible = false;
            dado2.Visible = false;
            dado3.Visible = false;
            dado4.Visible = false;
            dado5.Visible = false;

            Dados1 = gestorDado.IniciarDados(Dados1);



            label1.Text = jugador1.Usuario;
            label2.Text = jugador2.Usuario;

            timerTurno.Enabled = true;

            int contTurno = 0;
            bool usuario1Jugando = false;

            while (contTurno != 5)
            {
                if (!usuario1Jugando)
                {
                    label28.Text = jugador1.Usuario;
                }
                else
                {
                    label28.Text = jugador2.Usuario;
                }
                usuario1Jugando = !usuario1Jugando;
                contTurno++;
            
            }

        }
        int turnoJugador1 = 0;
        int turnoJugador2 = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            
            List<BE.Dado> dados;

            if (lanzamientos == 2)
            {
                buttonLanzar.Enabled = false;
            }
                    

            if (lanzamientos != 3)
            {
                int[] arrayPuntaje = new int[7];

                dado1.Visible = true;
                dado2.Visible = true;
                dado3.Visible = true;
                dado4.Visible = true;
                dado5.Visible = true;

                dados = gestor.LanzarDados(Dados1);

                int contador = 1;

                foreach (var item in dados)
                {
                    if (contador == 1)
                    {
                        dado1.EstablecerImagen(item.ValorNumero);
                        contador++;
                    }
                    else if (contador == 2)
                    {
                        dado2.EstablecerImagen(item.ValorNumero);
                        contador++;
                    }
                    else if (contador == 3)
                    {
                        dado3.EstablecerImagen(item.ValorNumero);
                        contador++;
                    }
                    else if (contador == 4)
                    {
                        dado4.EstablecerImagen(item.ValorNumero);
                        contador++;
                    }
                    else
                    {
                        dado5.EstablecerImagen(item.ValorNumero);
                        contador++;
                    }
                }

                if (label28.Text == jugador2.Usuario.ToString())
                {
                    turnoJugador2++;
                    button22.Visible = true;
                    button21.Visible = true;
                    button20.Visible = true;
                    button19.Visible = true;
                    button22.Text = "0";
                    button21.Text = "0";
                    button20.Text = "0";
                    button19.Text = "0";

                    if (gestorPuntaje.Generala(dados))
                    {
                        if (lanzamientos == 0)
                        {

                        }
                        else
                        {
                            button22.Text = "60";
                        }
                    }
                    else if (gestorPuntaje.Poker(dados))
                    {
                        if (lanzamientos == 0)
                        {
                            button21.Text = "45";
                        }
                        else
                        {
                            button21.Text = "40";
                        }
                        

                    }
                    else if (gestorPuntaje.Full(dados))
                    {
                        if (lanzamientos == 0)
                        {
                            button20.Text = "35";
                        }
                        else
                        {
                            button20.Text = "30";
                        }
                        
                    }
                    else if (gestorPuntaje.Escalera(dados))
                    {
                        if (lanzamientos == 0)
                        {
                            button19.Text = "25";
                        }
                        else
                        {
                            button19.Text = "20";
                        }
                        
                    }

                    arrayPuntaje = gestorPuntaje.CalcularPuntaje(dados);

                    for (int i = 0; i < arrayPuntaje.Length; i++)
                    {
                        if (i == 1)
                        {
                            button13.Visible = true;
                            button13.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 2)
                        {
                            button14.Visible = true;
                            button14.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 3)
                        {
                            button15.Visible = true;
                            button15.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 4)
                        {
                            button16.Visible = true;
                            button16.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 5)
                        {
                            button17.Visible = true;
                            button17.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else
                        {
                            button18.Visible = true;
                            button18.Text = (arrayPuntaje[i] * i).ToString();
                        }
                    }
                }
                else
                {
                    turnoJugador1++;
                    button11.Visible = true;
                    button10.Visible = true;
                    button9.Visible = true;
                    button8.Visible = true;
                    button11.Text = "0";
                    button10.Text = "0";
                    button9.Text = "0";
                    button8.Text = "0";


                    if (gestorPuntaje.Generala(dados))
                    {
                        if (lanzamientos == 0)
                        {

                        }
                        else
                        {
                            button11.Text = "60";
                        }
                    }
                    else if (gestorPuntaje.Poker(dados))
                    {
                        if (lanzamientos == 0)
                        {
                            button10.Text = "45";
                        }
                        else
                        {
                            button10.Text = "40";
                        }


                    }
                    else if (gestorPuntaje.Full(dados))
                    {
                        if (lanzamientos == 0)
                        {
                            button9.Text = "35";
                        }
                        else
                        {
                            button9.Text = "30";
                        }

                    }
                    else if (gestorPuntaje.Escalera(dados))
                    {
                        if (lanzamientos == 0)
                        {
                            button8.Text = "25";
                        }
                        else
                        {
                            button8.Text = "20";
                        }

                    }

                    arrayPuntaje = gestorPuntaje.CalcularPuntaje(dados);

                    for (int i = 0; i < arrayPuntaje.Length; i++)
                    {
                        if (i == 1)
                        {
                            button2.Visible = true;
                            button2.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 2)
                        {
                            button3.Visible = true;
                            button3.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 3)
                        {
                            button4.Visible = true;
                            button4.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 4)
                        {
                            button5.Visible = true;
                            button5.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else if (i == 5)
                        {
                            button6.Visible = true;
                            button6.Text = (arrayPuntaje[i] * i).ToString();
                        }
                        else
                        {
                            button7.Visible = true;
                            button7.Text = (arrayPuntaje[i] * i).ToString();
                        }
                    }
                }
                lanzamientos++;
            }
        }

        public void PasarTurno()
        {
            string jugadorActual = label28.Text;

            if (jugador1.Usuario.ToString() == jugadorActual)
            {
                label28.Text = jugador2.Usuario.ToString();
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button9.Visible = false;
                button10.Visible = false;
                button11.Visible = false;

                button2.Text = "0";
                button3.Text = "0";
                button4.Text = "0";
                button5.Text = "0";
                button6.Text = "0";
                button7.Text = "0";
                button8.Text = "0";
                button9.Text = "0";
                button10.Text = "0";
                button11.Text = "0";
            }
            else
            {
                label28.Text = jugador1.Usuario.ToString();

                button13.Visible = false;
                button14.Visible = false;
                button15.Visible = false;
                button16.Visible = false;
                button17.Visible = false;
                button18.Visible = false;
                button19.Visible = false;
                button20.Visible = false;
                button21.Visible = false;
                button22.Visible = false;

                button13.Text = "0";
                button14.Text = "0";
                button15.Text = "0";
                button16.Text = "0";
                button17.Text = "0";
                button18.Text = "0";
                button19.Text = "0";
                button20.Text = "0";
                button21.Text = "0";
                button22.Text = "0";
            }
            buttonLanzar.Enabled = true;
            lanzamientos = 0;
            conteo = 20;

            dado1.Visible = false;
            dado2.Visible = false;
            dado3.Visible = false;
            dado4.Visible = false;
            dado5.Visible = false;

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dado1_Load(object sender, EventArgs e)
        {

        }

        private void dado6_Click(object sender, EventArgs e)
        {

        }

        private void dado1_Load_1(object sender, EventArgs e)
        {

        }

        private void dado2_Load(object sender, EventArgs e)
        {

        }

        private void dado3_Load(object sender, EventArgs e)
        {

        }

        private void dado4_Load(object sender, EventArgs e)
        {

        }

        private void dado5_Load(object sender, EventArgs e)
        {

        }

        private void timerTurno_Tick(object sender, EventArgs e)
        {
            string jugadorPerdedor;
            if (conteo == 0)
            {
                timerTurno.Enabled = false;

                jugadorPerdedor = label28.Text;

                if (jugador1.Usuario == label28.Text)
                {
                    gestorJugador.Grabar(jugador1);
                }
                               
                MessageBox.Show("Te retiraste de la partida: " + jugadorPerdedor);
            }
            else
            {
                conteo--;
                labelTimer.Text = conteo.ToString();
            }
        }

        private void MesaJuego_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerTurno.Enabled = false;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            label12.Text = button11.Text;
            label12.Visible = true;
            button11.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label12.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button11.Text + " + " + " Generala");
            button11.Text = "0";
            PasarTurno();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            label11.Text = button10.Text;
            label11.Visible = true;
            button10.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label11.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button10.Text + " + " + " Poker");
            button10.Text = "0";
            PasarTurno();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            label10.Text = button9.Text;
            label10.Visible = true;
            button9.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label10.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button9.Text + " + " + " Full");
            button9.Text = "0";
            PasarTurno();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            label9.Text = button8.Text;
            label9.Visible = true;
            button8.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label9.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button8.Text + " + " + " Escalera");
            button8.Text = "0";
            PasarTurno();
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            label3.Text = button2.Text;
            label3.Visible = true;
            button2.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label3.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button2.Text + " al " + " 1");
            button2.Text = "0";
            PasarTurno();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label4.Text = button3.Text;
            label4.Visible = true;
            button3.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label4.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button3.Text + " al " + " 2");
            button3.Text = "0";
            PasarTurno();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            label5.Text = button4.Text;
            label5.Visible = true;
            button4.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label5.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button4.Text + " al " + " 3");
            button4.Text = "0";
            PasarTurno();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            label6.Text = button5.Text;
            label6.Visible = true;
            button5.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label6.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button5.Text + " al " + " 4");
            button5.Text = "0";
            PasarTurno();
        }

        private void button6_Click(object sender, EventArgs e)
        {

            label7.Text = button6.Text;
            label7.Visible = true;
            button6.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label7.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button6.Text + " al " + " 5");
            button6.Text = "0";
            PasarTurno();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            label8.Text = button7.Text;
            label8.Visible = true;
            button7.Enabled = false;
            label25.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label8.Text), int.Parse(label25.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button7.Text + " al " + " 6");
            button7.Text = "0";
            PasarTurno();
        }

        private void labelTimer_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            label14.Text = button13.Text;
            label14.Visible = true;
            button13.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label14.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button13.Text + " al " + " 1");
            button13.Text = "0";
            PasarTurno();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            label15.Text = button14.Text;
            label15.Visible = true;
            button14.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label15.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button14.Text + " al " + " 2");
            button14.Text = "0";
            PasarTurno();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            label16.Text = button15.Text;
            label16.Visible = true;
            button15.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label16.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button15.Text + " al " + " 3");
            button15.Text = "0";
            PasarTurno();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            label17.Text = button16.Text;
            label17.Visible = true;
            button16.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label17.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button16.Text + " al " + " 4");
            button16.Text = "0";
            PasarTurno();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            label18.Text = button17.Text;
            label18.Visible = true;
            button17.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label18.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button17.Text + " al " + " 5");
            button17.Text = "0";
            PasarTurno();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            label19.Text = button18.Text;
            label19.Visible = true;
            button18.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(button18.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button18.Text + " al " + " 6");
            button18.Text = "0";
            PasarTurno();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            label20.Text = button19.Text;
            label20.Visible = true;
            button19.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label20.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button19.Text + " + " + " Escalera");
            button19.Text = "0";
            PasarTurno();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            label21.Text = button20.Text;
            label21.Visible = true;
            button20.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label21.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button20.Text + " + " + " Full");
            button20.Text = "0";
            PasarTurno();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            label22.Text = button21.Text;
            label22.Visible = true;
            button21.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label22.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button21.Text + " + " + " Poker");
            button21.Text = "0";
            PasarTurno();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            label23.Text = button22.Text;
            label23.Visible = true;
            button22.Enabled = false;
            label26.Text = gestorPuntaje.ActualizarPuntaje(int.Parse(label23.Text), int.Parse(label26.Text)).ToString();
            listBox1.Items.Add(label28.Text + ": " + button22.Text + " + " + " Generala");
            button22.Text = "0";
            PasarTurno();
        }

    }
}
