﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Dado
    {
        public List<BE.Dado> IniciarDados(List<BE.Dado> dados)
        {
            for (int i = 1; i < 6; i++)
            {
                BE.Dado dado = new BE.Dado();
                dado.Flag = false;
                dado.Numero = i;
                dados.Add(dado);
            }
            return dados;
        }
    }
}
