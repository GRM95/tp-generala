﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugador
    {
        DAL.MP_JUGADOR mp = new DAL.MP_JUGADOR();

        public void Grabar(BE.Jugador jugador)
        {
            mp.Editar(jugador);
        }

        public void Borrar(BE.Jugador jugador)
        {
            mp.Borrar(jugador);
        }

        public bool Logear(BE.Jugador jugador)
        {
            return mp.Logear(jugador);
        }

        public List<BE.Jugador> Listar()
        {
          return  mp.Listar();

        }

    }
}
