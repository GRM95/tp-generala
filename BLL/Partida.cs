﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Partida
    {
        Turno turno = new Turno();
        public void IniciarPartida(BE.Jugador usuario1, BE.Jugador usuario2)
        {
            turno.IniciarTurnos(usuario1,usuario2);
        }
    }
}
