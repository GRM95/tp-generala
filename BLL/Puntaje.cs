﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Puntaje
    {
        private string nomPun;

        public bool Generala(List<BE.Dado> Dados)
        {
            bool Generala = false;
            int contador = 1;
            for (int i = 1; i < Dados.Count(); i++)
            {
                if (Dados[i].ValorNumero == Dados[0].ValorNumero)
                {
                    contador++;
                }
            }
            if (contador == 5)
            {
                Generala = true;
            }
            return Generala;
        }

        public bool Poker(List<BE.Dado> Dados)
        {
            int[] numeros = new int[7];
            int i = 0;
            bool pokerok = false;
            int numerocon4 = 0;
            int posicion = 0;
            for (i = 0; i < Dados.Count; i++)
            {
                if (Dados[i].ValorNumero == 1)
                {
                    numeros[1]++;
                }
                if (Dados[i].ValorNumero == 2)
                {
                    numeros[2]++;
                }
                if (Dados[i].ValorNumero == 3)
                {
                    numeros[3]++;
                }
                if (Dados[i].ValorNumero == 4)
                {
                    numeros[4]++;
                }
                if (Dados[i].ValorNumero == 5)
                {
                    numeros[5]++;
                }
                if (Dados[i].ValorNumero == 6)
                {
                    numeros[6]++;
                }
            }
            for (i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] == 4)
                {
                    pokerok = true;
                    numerocon4 = i;
                }
            }
            for (i = 0; i < Dados.Count; i++)
            {
                if (Dados[i].ValorNumero != numerocon4)
                {
                    posicion = i;
                }
            }
            return pokerok;
        }

        public bool Full(List<BE.Dado> Dados)
        {
            bool full = false;
            int[] numeros = new int[7];
            int i = 0;
            int numerocon3 = 0;
            int numerocon2 = 0;
            for (i = 0; i < Dados.Count; i++)
            {
                if (Dados[i].ValorNumero == 1)
                {
                    numeros[1]++;
                }
                if (Dados[i].ValorNumero == 2)
                {
                    numeros[2]++;
                }
                if (Dados[i].ValorNumero == 3)
                {
                    numeros[3]++;
                }
                if (Dados[i].ValorNumero == 4)
                {
                    numeros[4]++;
                }
                if (Dados[i].ValorNumero == 5)
                {
                    numeros[5]++;
                }
                if (Dados[i].ValorNumero == 6)
                {
                    numeros[6]++;
                }
            }
            for (i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] == 3)
                {
                    numerocon3 = i;
                }
                if (numeros[i] == 2)
                {
                    numerocon2 = i;
                }
            }
            if (numerocon3 != 0 && numerocon2 != 0)
            {
                full = true;
            }
            return full;
        }

        public bool Escalera(List<BE.Dado> Dados)
        {
            bool escaleraok = false;
            int[] numeros = new int[7];
            int i = 0;
            int escalera = 0;
            for (i = 0; i < Dados.Count; i++)
            {
                if (Dados[i].ValorNumero == 1)
                {
                    numeros[1]++;
                }
                if (Dados[i].ValorNumero == 2)
                {
                    numeros[2]++;
                }
                if (Dados[i].ValorNumero == 3)
                {
                    numeros[3]++;
                }
                if (Dados[i].ValorNumero == 4)
                {
                    numeros[4]++;
                }
                if (Dados[i].ValorNumero == 5)
                {
                    numeros[5]++;
                }
                if (Dados[i].ValorNumero == 6)
                {
                    numeros[6]++;
                }
            }
            if (numeros[1] == 1 && numeros[2] == 1 && numeros[3] == 1 && numeros[4] == 1 && numeros[5] == 1)
            {
                escalera = 1;
            }
            if (numeros[2] == 1 && numeros[3] == 1 && numeros[4] == 1 && numeros[5] == 1 && numeros[6] == 1)
            {
                escalera = 1;
            }
            if (escalera == 1)
            {
                escaleraok = true;
            }
            return escaleraok;
        }

        public int[] CalcularPuntaje(List<BE.Dado> Dados)
        {
            int[] numeros = new int[7];


            for (int i = 0; i < Dados.Count(); i++)
            {
                if (Dados[i].ValorNumero == 1)
                {
                    numeros[1]++;
                }
                if (Dados[i].ValorNumero == 2)
                {
                    numeros[2]++;
                }
                if (Dados[i].ValorNumero == 3)
                {
                    numeros[3]++;
                }
                if (Dados[i].ValorNumero == 4)
                {
                    numeros[4]++;
                }
                if (Dados[i].ValorNumero == 5)
                {
                    numeros[5]++;
                }
                if (Dados[i].ValorNumero == 6)
                {
                    numeros[6]++;
                }
            }

            return numeros;
        }

        public int ActualizarPuntaje(int puntajeObtenido , int puntajeTotalActual)
        {
            puntajeTotalActual += puntajeObtenido;
            return puntajeTotalActual;
        }
    }
}
